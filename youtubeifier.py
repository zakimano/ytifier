import subprocess
import argparse


# argument handling

s_desc = "Make .pls playlists from youtube playlists.\nRecommended to play with mpv.\nProcessing URLs tend to take a long time, please be patient. This script is unlikely to hang, it will exit if it detects a failure."
parser = argparse.ArgumentParser(description=s_desc)

parser.add_argument("output_file", default=None, help="The string to use as file name; .pls extension appended automagically.")
parser.add_argument("youtube_url", default=None, help="A youtube-playlist url will result in the same playlist in the output file.")

args = parser.parse_args()

# end of argparse, here comes main


def main():
	
	print("Processing url: " + args.youtube_url + "\n(This may take a (very) long time.)")
	
	ytdlcmd = "youtube-dl -i --get-id " + args.youtube_url
	
	ytdlproc = subprocess.Popen(ytdlcmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	out, err = ytdlproc.communicate()
	out = out.decode()
	err = err.decode()
	
	# ytdl ran, time to examine output
	
	if err is not "" and out is "":
		print("You fool! Here's an error for you:\n" + err)
	elif err is not "" and out is not "":
		print("Something got borked...\n" + err)
	elif err is "" and out is not "":
		print("Nicely done!")
	else:
		print("Something definetely went wrong. Exiting.")
		exit()
	
	# if we got proper output, we print that to a file.
	
	if out is not "":
		print("Writing to file: " + args.output_file)
		with open(args.output_file, "w") as opf:
			opf.write("[playlist]\n\n")
			for line in out.split():
				opf.write("File=https://www.youtube.com/watch?v=" + line + "\n")
	
	pass


main()
